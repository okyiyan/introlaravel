<!DOCTYPE html>
<html>
    <head>
        <title>
            Tugas Hari 1 Oky Iyan Pratama
        </title>
        <body>
            <h1>Buat Akun Baru!</h1>
            <h2>Sign Up Form</h2>
            <form action="/welcome" method="POST">
                @csrf
                <label for="fname">First name:</label><br><br>
                <input type="text" id = "fname" name = "fname"><br><br>

                <label for="lname">Last name:</label><br><br>
                <input type="text" id = "lname" name = "lname"><br><br>
                
                <label for="gender">Gender:</label><br><br>
                <input type="radio" id = "gender" name = "gender" value="Male">
                <label for="gender">Male</label><br>
                <input type="radio" id = "gender" name = "gender" value="Female">
                <label for="gender">Female</label><br>
                <input type="radio" id = "gender" name = "gender" value="Other">
                <label for="gender">Other</label><br><br>
                
                <label for="nasionality">Nasionality:</label><br><br>
                <select>
                    <option value="Indonesian">Indonesian</option>
                    <option value="Malaysian">Malaysian</option>
                    <option value="Singaporean">Singaporean</option>
                </select><br><br>
                
                <label for="language">Language spoken:</label><br><br>
                <input type="checkbox" id = "language" name = "language1" value="Bahasa Indonesia">
                <label for="language1">Bahasa Indonesia</label><br>
                <input type="checkbox" id = "language" name = "language2" value="English">
                <label for="language2">English</label><br>
                <input type="checkbox" id = "language" name = "language3" value="Other">
                <label for="language3">Other</label><br><br><br>
                
                <label for="bio">Bio:</label><br><br>
                <textarea name="bio" rows="10" cols="30"></textarea><br>
                
                <input type="submit" value="Sign Up"> 
            </form>
        </body>
    </head>
</html>