<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function greeting(Request $request){
        //dd($request->all());
        $fullname = strtoupper($request["fname"])." ".strtoupper($request["lname"]);
        return view('greeting',compact ('fullname'));
    }
}
